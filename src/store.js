import {
  configureStore,
  createSlice,
  createAsyncThunk,
} from "@reduxjs/toolkit";

const initialState = {
  modal: {
    isOpen: false,
    selectedProduct: null,
    modalType: null,
  },
  products: [],
  cart: JSON.parse(localStorage.getItem("cart")) || [],
  favorites: JSON.parse(localStorage.getItem("favorites")) || {},
};

export const fetchProducts = createAsyncThunk(
  "products/fetchProducts",
  async () => {
    const response = await fetch("/data.json");
    if (!response.ok) {
      throw new Error("Failed to fetch products");
    }
    const data = await response.json();
    return data;
  }
);

const modalSlice = createSlice({
  name: "modal",
  initialState: initialState.modal,
  reducers: {
    openModal: (state, action) => {
      state.isOpen = true;
      state.selectedProduct = action.payload;
      state.modalType = action.payload.modalType;
    },
    closeModal: (state) => {
      state.isOpen = false;
      state.selectedProduct = null;
      state.modalType = null;
    },
  },
});

const productsSlice = createSlice({
  name: "products",
  initialState: initialState.products,
  extraReducers: (builder) => {
    builder
      .addCase(fetchProducts.fulfilled, (state, action) => {
        return action.payload;
      })
      .addDefaultCase((state) => {
        return state;
      });
  },
});

const cartSlice = createSlice({
  name: "cart",
  initialState: initialState.cart,
  reducers: {
    addToCart: (state, action) => {
      const product = action.payload;

      if (!state.find((item) => item.SKU === product.SKU)) {
        state.push(product);
        localStorage.setItem("cart", JSON.stringify(state));
      }
    },
    removeFromCart: (state, action) => {
      const updatedCart = state.filter((item) => item.SKU !== action.payload);
      localStorage.setItem("cart", JSON.stringify(updatedCart));
      return updatedCart;
    },
    clearCart: (state) => {
      localStorage.removeItem("cart");
      return [];
    },
  },
});

const favoritesSlice = createSlice({
  name: "favorites",
  initialState: initialState.favorites,
  reducers: {
    toggleFavorite: (state, action) => {
      const productSKU = action.payload;
      if (state[productSKU]) {
        delete state[productSKU];
      } else {
        state[productSKU] = true;
      }
      localStorage.setItem("favorites", JSON.stringify(state));
    },
  },
});

export const { openModal, closeModal } = modalSlice.actions;
export const { addToCart, removeFromCart, clearCart } = cartSlice.actions;
export const { toggleFavorite } = favoritesSlice.actions;

const store = configureStore({
  reducer: {
    modal: modalSlice.reducer,
    products: productsSlice.reducer,
    cart: cartSlice.reducer,
    favorites: favoritesSlice.reducer,
  },
});

export default store;
