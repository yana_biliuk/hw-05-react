import { createBrowserRouter } from "react-router-dom";

import Layout from "./Layout";
import CartPage from "./Pages/CartPage";
import HomePage from './Pages/HomePage';
import FavoritePage from './Pages/FavoritePage'

export const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    children: [
      {
        index: true,
        element: <HomePage />,
      },

      {
        path: "/favorite",
        element: <FavoritePage />,
      },
      {
        path: "/cart",
        element: <CartPage />,
      },
    ],
  },
]);
