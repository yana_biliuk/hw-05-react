import styled from "styled-components";
import ModalWrapper from "./ModalWrapper/ModalWrapper";
import PropTypes from "prop-types";

const Container = styled.div`
  padding: 56px 56px;
  border-radius: 16px;
  background: #fff;
  box-shadow: 0px 4px 4px 0px rgba(0, 0, 0, 0.25);
  position: relative;
`;

function Modal({ children, onClose }) {
  return (
    <ModalWrapper onClose={onClose}>
      <Container>{children}</Container>
    </ModalWrapper>
  );
}
Modal.propTypes = {
  children: PropTypes.node.isRequired,
  onClose: PropTypes.func,
};

export default Modal;
