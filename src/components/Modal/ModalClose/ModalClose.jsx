import styled from "styled-components";
import PropTypes from "prop-types";

const CloseModal = styled.button`
  position: absolute;
  top: 10px;
  right: 10px;
  background: none;
  border: none;
  font-size: 24px;
  cursor: pointer;
`;

function ModalClose({ onClick }) {
  return <CloseModal onClick={onClick}>&times;</CloseModal>;
}
ModalClose.propTypes = {
  onClick: PropTypes.func,
};

export default ModalClose;
