import Button from "../../Button/Button";
import styled from "styled-components";
import PropTypes from "prop-types";

const Footer = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 64px;
  gap: 64px;
`;

function ModalFooter({ firstText, secondaryText, firstClick, secondaryClick }) {
  return (
    <Footer>
    {firstText && <Button onClick={firstClick}>{firstText}</Button>}
    {secondaryText && (
      <Button onClick={secondaryClick}>{secondaryText}</Button>
    )}
  </Footer>


  );
}
ModalFooter.propTypes = {
  onClick: PropTypes.func,
};

export default ModalFooter;
