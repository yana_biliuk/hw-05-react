import styled from "styled-components";
import PropTypes from "prop-types";

const Wrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: rgba(0, 0, 0, 0.5);
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 1000;
`;

function ModalWrapper({ children, onClose }) {
  const closeWrapper = (event) => {
    if (event.target === event.currentTarget) {
      onClose();
    }
  };
  return <Wrapper onClick={closeWrapper}>{children}</Wrapper>;
}

ModalWrapper.propTypes = {
  children: PropTypes.node.isRequired,
  onClose: PropTypes.func,
};
export default ModalWrapper;
