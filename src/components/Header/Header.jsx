import styled from "styled-components";
import { FaShoppingCart, FaStar } from "react-icons/fa";
import PropTypes from "prop-types";

const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 10px 20px;
  background-color: #f8f8f8;
  border-bottom: 1px solid #ddd;
`;

const IconContainer = styled.div`
  display: flex;
  align-items: center;
`;

const IconWrapper = styled.div`
  position: relative;
  margin: 0 20px;
  cursor: pointer;
`;

const Counter = styled.div`
  position: absolute;
  top: -5px;
  right: -10px;
  background: red;
  color: white;
  border-radius: 50%;
  padding: 2px 6px;
  font-size: 12px;

`;
// const StyledFaShoppingCart = styled(FaShoppingCart)`
//   color: ${props => props.isFavorite ? 'gold' : 'inherit'};
// `;

// const StyledFaStar = styled(FaStar)`
//   color: ${props => props.isFavorite ? 'gold' : 'inherit'};
// `;
const StarIcon = styled(FaStar)`
  color: ${props => props.isFavorite ? "darkviolet" : "#ccc"};
`;
const ShopIcon = styled(FaShoppingCart)`
  color: ${props => props.isFavorite ? "darkviolet" : "#ccc"};
`;

function Header({
  cartCount,
  favoritesCount,
  isFavoriteCart,
  isFavoriteFavorites,
  children,
}) {
  return (
    <HeaderContainer>
      <IconContainer>
        <IconWrapper>
          <ShopIcon isFavorite={isFavoriteCart} size={24} />
          <Counter>{cartCount}</Counter>
        </IconWrapper>
      </IconContainer>
{children}
      <IconContainer>
        <IconWrapper>
          <StarIcon isFavorite={isFavoriteFavorites} size={24} />
          <Counter>{favoritesCount}</Counter>
        </IconWrapper>
      </IconContainer>
    </HeaderContainer>
  );
}
Header.propTypes = {
  children: PropTypes.node.isRequired,
  cartCount: PropTypes.number.isRequired,
  favoritesCount: PropTypes.number.isRequired,
  isFavoriteCart: PropTypes.bool.isRequired,
  isFavoriteFavorites: PropTypes.bool.isRequired,
};

export default Header;
