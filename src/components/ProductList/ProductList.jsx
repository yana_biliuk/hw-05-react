import ProductCard from "../ProductCard/ProductCar";
import styled from "styled-components";
import PropTypes from "prop-types";

const ProductListContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

function ProductList({
  products,
  handleAddToCart,
  handleToggleFavorite,

  favorites,
  showButton,
  showRemoveIcon,
  handleRemoveFromCart,
  cart,
}) {
  return (
    <ProductListContainer>
      {products.map((product, index) => (
        <ProductCard
          handleAddToCart={handleAddToCart}
          key={index}
          product={product}
          handleToggleFavorite={handleToggleFavorite}
          isFavorite={favorites[product.SKU]}
          handleRemoveFromCart={handleRemoveFromCart}
          showButton={showButton}
          showRemoveIcon={showRemoveIcon}
          cart ={cart}
        />
      ))}
    </ProductListContainer>
  );
}
ProductList.propTypes = {
  products: PropTypes.arrayOf(PropTypes.shape({
    ImageUrl: PropTypes.string.isRequired,
    Name: PropTypes.string.isRequired,
    Price: PropTypes.number.isRequired,
    Color: PropTypes.string.isRequired,
    SKU: PropTypes.string.isRequired,
  })).isRequired,
  handleAddToCart: PropTypes.func.isRequired,
  handleToggleFavorite: PropTypes.func.isRequired,
  // favorites: PropTypes.object.isRequired,
  favorites: PropTypes.objectOf(PropTypes.bool).isRequired, 
  showButton: PropTypes.bool.isRequired,
  showRemoveIcon: PropTypes.bool.isRequired,
  handleRemoveFromCart: PropTypes.func.isRequired,
};

export default ProductList;
