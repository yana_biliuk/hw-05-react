import styled from "styled-components";
import Button from "../Button/Button";
import { FaStar } from "react-icons/fa";
import PropTypes from "prop-types";
import { FaTimes } from "react-icons/fa";

const Card = styled.div`
  border: 1px solid #ddd;
  border-radius: 8px;
  padding: 16px;
  margin: 16px;
  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
  text-align: center;
  background-color: #fff;
  display: inline-block;
  position: relative;
`;

const ProductImage = styled.img`
  width: 150px;
  height: 150px;
  object-fit: cover;
  border-radius: 8px;
  margin-bottom: 16px;
`;

const ProductName = styled.h2`
  font-size: 1.5em;
  margin-bottom: 8px;
`;

const ProductDetails = styled.p`
  font-size: 1em;
  margin: 5px 0;
`;

const ButtonContainer = styled.div`
  margin-top: 20px;
`;
const StarIcon = styled(FaStar)`
  position: absolute;
  top: 16px;
  right: 16px;
  cursor: pointer;
  color: ${(props) => (props.isFavorite ? "darkviolet" : "#ccc")};
`;

const RemoveIcon = styled(FaTimes)`
  position: absolute;
  top: 10px;
  right: 10px;
  cursor: pointer;
  font-size: 20px;
  color: #ccc;
  &: hover {
    color: darkviolet;
  }
`;

function ProductCard({
  product,
  handleAddToCart,
  handleToggleFavorite,
  isFavorite,
  showButton,
  showRemoveIcon,
  handleRemoveFromCart,
  cart,
}) {
const isInCart = cart.find(item => item.SKU === product.SKU) !== undefined;

  return (
    <Card>
      {showRemoveIcon ? (
        <RemoveIcon onClick={() => handleRemoveFromCart(product)} />
      ) : (
        <StarIcon
          onClick={() => handleToggleFavorite(product.SKU)}
          isFavorite={isFavorite}
        />
      )}
      <ProductImage
        src={product.ImageUrl}
        alt={product.Name}
        style={{ width: "100px" }}
      />
      <ProductName>{product.Name}</ProductName>
      <ProductDetails>Price: {product.Price}грн</ProductDetails>
      <ProductDetails>Color: {product.Color}</ProductDetails>
      <ProductDetails>Article: {product.SKU}</ProductDetails>

      {showButton && (
        <ButtonContainer>
          <Button onClick={() => handleAddToCart(product)} disabled ={isInCart}> Add to cart</Button>
        </ButtonContainer>
      )}
    </Card>
  );
}

ProductCard.propTypes = {
  product: PropTypes.shape({
    ImageUrl: PropTypes.string.isRequired,
    Name: PropTypes.string.isRequired,
    Price: PropTypes.number.isRequired,
    Color: PropTypes.string.isRequired,
    SKU: PropTypes.string.isRequired,
  }).isRequired,
  handleAddToCart: PropTypes.func.isRequired,
  handleToggleFavorite: PropTypes.func.isRequired,
  handleRemoveFromCart: PropTypes.func.isRequired,
  // isFavorite: PropTypes.bool.isRequired,
  showButton: PropTypes.bool.isRequired,
  showRemoveIcon: PropTypes.bool.isRequired,
};

export default ProductCard;
