import { useFormik } from "formik";
import * as Yup from "yup";
import styled from "styled-components";
import { clearCart } from "../../store";
import { useDispatch, useSelector } from "react-redux";

const StyledForm = styled.form`
  display: flex;
  flex-direction: column;
  gap: 20px;
  width: 400px;
  margin: 0 auto;
  padding: 20px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
`;

const FormField = styled.div`
  display: flex;
  flex-direction: column;
  label {
    margin-bottom: 5px;
    font-weight: bold;
  }
  input {
    padding: 10px;
    font-size: 16px;
    border: 1px solid #ccc;
    border-radius: 4px;
    transition: border-color 0.3s, box-shadow 0.3s;
    &:hover {
      border-color: darkviolet;
      box-shadow: 0 0 5px rgba(148, 0, 211, 0.5);
    }

    &:focus {
      outline: none;
      border-color: darkviolet;
      box-shadow: 0 0 5px rgba(148, 0, 211, 0.5);
    }
  }
`;

const ErrorMessage = styled.div`
  color: red;
  text-align: center;
  margin-top: 5px;
  font-size: 14px;
`;

const SubmitButton = styled.button`
  padding: 12px 24px;
  font-size: 16px;
  color: white;
  background-color: darkviolet;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  transition: background-color 0.3s, transform 0.3s;

  &:hover {
    background-color: green;
    transform: translateY(-2px);
  }

  &:focus {
    outline: none;
    background-color: green;
  }
`;

const FormCart = ({ onOrder }) => {
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.cart);
  const formik = useFormik({
    initialValues: {
      name: "",
      lastName: "",
      age: "",
      address: "",
      phoneNumber: "",
    },
    validationSchema: Yup.object({
      name: Yup.string().required("Required"),
      lastName: Yup.string().required("Required"),
      age: Yup.number()
        .required("Required")
        .positive("Age must be a positive number")
        .integer("Age must be an integer"),
      address: Yup.string().required("Required"),
      phoneNumber: Yup.string()

        .matches(/^[0-9]{10}$/, "Phone number must be exactly 10 digits")
        .required("Required"),
    }),
    onSubmit: (values, { resetForm }) => {
      console.log("Purchased products:", cart);
      console.log("User information:", values);
      dispatch(clearCart());
      resetForm();
      onOrder();
    },
  });
  return (
    <StyledForm onSubmit={formik.handleSubmit}>
      <FormField>
        <label htmlFor="name">Name</label>
        <input
          id="name"
          name="name"
          type="text"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.name}
        />
        {formik.touched.name && formik.errors.name ? (
          <ErrorMessage>{formik.errors.name}</ErrorMessage>
        ) : null}
      </FormField>
      <FormField>
        <label htmlFor="lastName">Last Name</label>
        <input
          id="lastName"
          name="lastName"
          type="text"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.lastName}
        />
        {formik.touched.lastName && formik.errors.lastName ? (
          <ErrorMessage>{formik.errors.lastName}</ErrorMessage>
        ) : null}
      </FormField>

      <FormField>
        <label htmlFor="age">Age</label>
        <input
          id="age"
          name="age"
          type="number"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.age}
        />
        {formik.touched.age && formik.errors.age ? (
          <ErrorMessage>{formik.errors.age}</ErrorMessage>
        ) : null}
      </FormField>

      <FormField>
        <label htmlFor="address">Adddress</label>
        <input
          id="address"
          name="address"
          type="text"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.address}
        />
        {formik.touched.address && formik.errors.address ? (
          <ErrorMessage>{formik.errors.address}</ErrorMessage>
        ) : null}
      </FormField>

      <FormField>
        <label htmlFor="phoneNumber">Number Phone</label>
        <input
          id="phoneNumber"
          name="phoneNumber"
          type="text"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.phoneNumber}
        />
        {formik.touched.phoneNumber && formik.errors.phoneNumber ? (
          <ErrorMessage>{formik.errors.phoneNumber}</ErrorMessage>
        ) : null}
      </FormField>

      <SubmitButton type="submit">Checkout</SubmitButton>
    </StyledForm>
  );
};

export default FormCart;
