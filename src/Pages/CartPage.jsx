import { useOutletContext } from "react-router-dom";
import styled from "styled-components";
import ModalImage from "../components/Modal/ModalImage/ModalImage";

import ProductCard from "../components/ProductCard/ProductCar";
import { useDispatch, useSelector } from "react-redux";
import { closeModal, openModal, removeFromCart } from "../store";
import FormCart from "../components/Form/Form";
import { useState } from "react";

const CartContainer = styled.div`
  padding: 20px;
  max-width: 1200px;
  margin: 0 auto;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const CartHeader = styled.h1`
  text-align: center;
  margin-bottom: 40px;
`;

const CartEmpty = styled.p`
  font-size: 18px;
  text-align: center;
  color: #888;
`;
const CartItemsContainer = styled.div`
  flex: 2;
  margin-right: 20px;
`;

const CartItems = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 20px;
`;
const FormContainer = styled.div`
  flex: 1;
  max-width: 400px;
  margin-top: 50px;
`;

function CartPage() {
  const { handleAddToCart, handleToggleFavorite } = useOutletContext();
  const dispatch = useDispatch();

  const cart = useSelector((state) => state.cart);
  const modal = useSelector((state) => state.modal);
  const favorites = useSelector((state) => state.favorites);
  const [order, setOrder] = useState(false);

  const handleRemoveClick = (product) => {
    dispatch(openModal({ ...product, modalType: "image" }));
  };

  const handleConfirmRemove = () => {
    dispatch(removeFromCart(modal.selectedProduct.SKU));
    dispatch(closeModal());
  };

  const handleCloseModal = () => {
    dispatch(closeModal());
  };
  const handleOrder = () => {
    setOrder(true);
  };

  return (
    <CartContainer>
      <CartItemsContainer>
        <CartHeader>Your cart</CartHeader>
        {order ? (
          <CartEmpty>Thank you for your order!</CartEmpty>
        ) : cart.length === 0 ? (
          <CartEmpty>Your cart is empty</CartEmpty>
        ) : (
          <CartItems>
            {cart.map((product, index) => (
              <ProductCard
                key={index}
                product={product}
                handleAddToCart={handleAddToCart}
                handleToggleFavorite={handleToggleFavorite}
                handleRemoveFromCart={handleRemoveClick}
                showRemoveIcon={true}
                favorites={favorites}
                showButton={false}
                cart={cart}
              />
            ))}
          </CartItems>
        )}
      </CartItemsContainer>
      <FormContainer>
        <FormCart onOrder={handleOrder} />
      </FormContainer>

      {modal.isOpen && modal.modalType === "image" && (
        <ModalImage
          onClose={handleCloseModal}
          onConfirm={handleConfirmRemove}
          product={modal.selectedProduct}
        />
      )}
    </CartContainer>
  );
}

export default CartPage;
