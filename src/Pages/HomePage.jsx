import { useOutletContext } from "react-router-dom";
import ProductList from "../components/ProductList/ProductList";

import styled from "styled-components";
import { useSelector } from "react-redux";
const Container = styled.div`
  max-width: 1200px;
  margin: 0 auto;
  padding: 20px;
`;

const Title = styled.h1`
  text-align: center;
  margin-bottom: 40px;
`;

function HomePage() {
  const { handleAddToCart, handleToggleFavorite } = useOutletContext();
  const products = useSelector((state) => state.products);
  const cart = useSelector((state) => state.cart);
  const favorites = useSelector((state) => state.favorites);
  return (
    <Container>
      <Title>Products</Title>
      <ProductList
        handleAddToCart={handleAddToCart}
        products={products}
        handleToggleFavorite={handleToggleFavorite}
        favorites={favorites}
        showButton={true}
        cart={cart}
      />
    </Container>
  );
}

export default HomePage;
