import { Outlet, Link } from "react-router-dom";
import Header from "./components/Header/Header";
import ModalText from "./components/Modal/ModalText/ModalText";
import { useEffect } from "react";

import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import {
  addToCart,
  closeModal,
  fetchProducts,
  openModal,
  toggleFavorite,
} from "./store";

const MenuItem = styled(Link)`
  display: inline-block;
  text-decoration: none;
  font-size: 24px;
  padding: 20px 30px;
  color: black;
  &: hover {
    color: darkviolet;
    cursor: pointer;
  }
`;

function Layout() {
  const dispatch = useDispatch();

  
  const isModalOpen = useSelector((state) => state.modal.isOpen);
  const modalType = useSelector((state) => state.modal.modalType);
  const selectedProduct = useSelector((state) => state.modal.selectedProduct);
  const products = useSelector((state) => state.products);
  const cart = useSelector((state) => state.cart);
  const favorites = useSelector((state) => state.favorites);

  useEffect(() => {
    dispatch(fetchProducts());
  }, [dispatch]);

  // Додати товар до корзини
  const handleAddToCart = (product) => {
    dispatch(openModal({ ...product, modalType: "text" }));
  };

  // Підтвердити додавання товару до корзини
  const handleConfirmAddToCart = () => {
    dispatch(addToCart(selectedProduct));
    dispatch(closeModal());
  };

  // Перемикач вибраного товару
  const handleToggleFavorite = (productSKU) => {
    dispatch(toggleFavorite(productSKU));
  };

  // Закриття модального вікна
  const handleCloseModal = () => {
    dispatch(closeModal());
  };

  const cartCount = cart.length;
  const favoritesCount = Object.values(favorites).filter(
    (favorite) => favorite
  ).length;

  return (
    <>
      <Header
        isFavoriteCart={cartCount > 0}
        isFavoriteFavorites={favoritesCount > 0}
        cartCount={cartCount}
        favoritesCount={favoritesCount}
      >
        <div>
          <MenuItem className="menu__item" to="/">
            HomePage
          </MenuItem>
          <MenuItem to="/cart">Cart</MenuItem>
          <MenuItem to="/favorite">Favorites</MenuItem>
        </div>
      </Header>
      <Outlet
        context={{
          products,
          handleAddToCart,
          handleToggleFavorite,
          favorites,
          isModalOpen,
          selectedProduct,
          handleCloseModal,
          cart,
        }}
      />
      {isModalOpen && modalType === "text" && (
        <ModalText
          onClose={handleCloseModal}
          product={selectedProduct}
          onComfirm={handleConfirmAddToCart}
        />
      )}
    </>
  );
}

export default Layout;
